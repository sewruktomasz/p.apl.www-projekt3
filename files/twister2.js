
var canvas = document.getElementById('twister');

var ctxX = canvas.width / 2;
var ctxY = canvas.height / 2;
var ctxRadius = canvas.width / 2.5;
var ctxStartAngle = Math.PI * 0;
var ctxFinishAngle = Math.PI * 2;

var howManyCircles = 16;
var smallCirclesRadius = canvas.width / 14;


function getPoint(c1, c2, radius, startAngle, finishAngle) {
  return [Math.round(c1 + Math.cos(finishAngle) * radius), Math.round(c2 + Math.sin(finishAngle) * radius)];
}




ctx = canvas.getContext('2d');
ctx.beginPath();
ctx.arc(ctxX, ctxY, ctxRadius, ctxStartAngle, ctxFinishAngle);
ctx.stroke();
document.getElementById('komunikat').innerHTML = "<div>" + getPoint(ctxX, ctxY, ctxRadius, ctxStartAngle, ctxFinishAngle) + " big</div>";




for (var i = 1; i <= howManyCircles; i++) {
  //circles
  var coordinates = getPoint(ctxX, ctxY, ctxRadius, ctxStartAngle, (ctxFinishAngle / howManyCircles) * i + Math.PI * 1 / howManyCircles);
  //document.write(coordinates + "<br />");
  ctx = canvas.getContext('2d');
  ctx.beginPath();
  ctx.arc(coordinates[0], coordinates[1], smallCirclesRadius, ctxStartAngle, ctxFinishAngle);
  ctx.strokeStyle = "blue";
  if (i % 4 == 0) ctx.fillStyle = "#77DF00";
  if (i % 4 == 1) ctx.fillStyle = "#FFF55B";
  if (i % 4 == 2) ctx.fillStyle = "#45ADE9";
  if (i % 4 == 3) ctx.fillStyle = "#EF1010";

  ctx.fill();
  ctx.stroke();


  //lines
  coordinates = getPoint(ctxX, ctxY, ctxRadius, ctxStartAngle, (ctxFinishAngle / howManyCircles) * i);

  var ctx = canvas.getContext("2d");
  ctx.beginPath();
  ctx.moveTo(ctxX, ctxY);
  ctx.lineTo(coordinates[0], coordinates[1]);
  ctx.strokeStyle = "red";
  ctx.fillStyle = "green";
  ctx.fill();
  ctx.stroke();
}



