
var sun = new Image();
var arrow = new Image();
var arrowSpeed = 0.0;
var arrowAngle = 0 //getRandomInt(0, 360);
var actualAngle;
var speedUpFlag = true;
function init() {
  sun.src = 'https://mdn.mozillademos.org/files/1456/Canvas_sun.png';

  arrow.src = './files/arrow.png';
  window.requestAnimationFrame(draw);
}

function draw() {
  var ctx = document.getElementById('canvas').getContext('2d');

  var canvasHeight = ctx.height;
  var canvasWidth = ctx.width;


  ctx.globalCompositeOperation = 'destination-over';
  ctx.clearRect(0, 0, 300, 300); // clear canvas

  ctx.save();
  ctx.translate(150, 150);

  // arrow
  var time = new Date();
  // ctx.rotate( ( ((2 * Math.PI) / 6) * time.getSeconds() + ((2 * Math.PI) / 6000) * time.getMilliseconds()  )*arrowSpeed);
  actualAngle = arrowAngle * Math.PI / 180 * arrowSpeed;

  ctx.rotate(actualAngle);
  arrowAngle += arrowSpeed;

  ctx.translate(0, 0);
  ctx.drawImage(arrow, -arrow.width / 2, -arrow.height / 1.25);


  ctx.restore();


  ctx.drawImage(sun, 0, 0, 300, 300);

  window.requestAnimationFrame(draw);
  document.getElementById('speed').innerHTML = "Speed: " + arrowSpeed;
  document.getElementById('angle').innerHTML = "Angle: " + actualAngle;
  document.getElementById('flag').innerHTML = "Flag: "+ speedUpFlag;
  return true;
}

init();

function spinNow2() {
  if (arrowSpeed >= 1.5) {
    speedUpFlag = false;
  }

  if(arrowSpeed<0){
    speedUpFlag=true;
  }

  if (speedUpFlag == true) {
    arrowSpeed = Math.round((arrowSpeed + 0.1) * 100) / 100;
  }
  else {
      arrowSpeed = arrowSpeed - 0.1;
  }
}






function spinNow() {
  if (arrowSpeed > 1.5) {
    toStop();
  }
  arrowSpeed = Math.round((arrowSpeed + 0.1) * 100) / 100;
  return true;
}

function toStop() {
  arrowSpeed = Math.round((arrowSpeed - 0.05) * 100) / 100;
  setTimeout(toStop, 400);
  if (arrowSpeed <= 0) {
    arrowSpeed = 0;
  }
  return true;

}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
