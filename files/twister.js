
var ctx = document.getElementById('canvas').getContext('2d');
var canvas = document.getElementById('canvas');
//arrow
var arrow = new Image();
var arrowSpeed = 1;
var arrowAngle = 1.0 //getRandomInt(0, 360);
var actualAngle;
var speedUpFlag = true;
var randomAngle = 350 * getRandomInt(0, 360);


//shield
var ctxX = canvas.width / 2;
var ctxY = canvas.height / 2;
var canvasHeight = ctx.height;
var canvasWidth = ctx.width;
var ctxRadius = canvas.width / 2.5;
var ctxStartAngle = Math.PI * 0;
var ctxFinishAngle = Math.PI * 2;


// var howManyCircles = 16;

// var howManyZones = 4;


var smallCirclesRadius = canvas.width / 14;




function init() {


  arrow.src = './files/arrow.png';
  window.requestAnimationFrame(draw);
}

function draw() {
  var howManyCircles = parseInt(document.getElementById('howManyCircles').value);
  var howManyZones = parseInt(document.getElementById('howManyZones').value);
  var listOfColors = ["#77DF00", "#FFF55B", "#45ADE9", "#EF1010"];

  try {
    var listOfColors = [];
    for (var color = 0; color < howManyZones; color++) {
      listOfColors.push(document.getElementById('color' + color).value);
      //document.write(document.getElementById('color1').value);
    }
  }
  catch(error){
    console.log(error);
  }
  ctx = canvas.getContext('2d');


  ctx.clearRect(0, 0, 700, 700); // clear canvas

  ctx.beginPath();
  ctx.arc(ctxX, ctxY, ctxRadius, ctxStartAngle, ctxFinishAngle);
  ctx.stroke();


  ctx.save();
  ctx.translate(ctxX, ctxY);//tuuuuuuu





  // arrow
  actualAngle = (arrowAngle * Math.PI / 180) * arrowSpeed;
  ctx.rotate(actualAngle);
  arrowAngle *= arrowSpeed;


  if (arrowAngle > randomAngle) {
    //arrowAngle = 0;
    speedUpFlag = false;
    arrowAngle = randomAngle;
  }
  if (arrowAngle < 0.01) {
    arrowSpeed = 1;
    ctx.rotate(getRandomInt(0, 360) * Math.PI / 180 * arrowSpeed);
    return true;
  }

  ctx.translate(0, 0);
  ctx.drawImage(arrow, -arrow.width / 2, -arrow.height / 1.25);
  ctx.restore();



  for (var i = 1; i <= howManyCircles; i++) {
    //circles
    var coordinates = getPoint(ctxX, ctxY, ctxRadius, ctxStartAngle, (ctxFinishAngle / howManyCircles) * i + Math.PI * 1 / howManyCircles);
    ctx.fillStyle = 'green';
    //ctx.fill();


    ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.arc(coordinates[0], coordinates[1], smallCirclesRadius, ctxStartAngle, ctxFinishAngle);
    ctx.strokeStyle = "blue";



    for (var j = 0; j < howManyZones; j++) {
      if (i % howManyZones == j) ctx.fillStyle = listOfColors[j];
    }

    ctx.fill();
    ctx.stroke();


    //lines
    coordinates = getPoint(ctxX, ctxY, ctxRadius, ctxStartAngle, (ctxFinishAngle / howManyCircles) * i);

    var ctx = canvas.getContext("2d");
    ctx.beginPath();
    ctx.moveTo(ctxX, ctxY);
    ctx.lineTo(coordinates[0], coordinates[1]);
    ctx.strokeStyle = "red";
    ctx.fillStyle = "green";
    ctx.fill();
    ctx.stroke();
  }





  window.requestAnimationFrame(draw);
  return true;

}

init();


function spinNow() {
  valuesReset();
  // init();
  if (speedUpFlag == true) {
    // arrowSpeed= arrowSpeed* 1.1;
    arrowSpeed = 1.035;
  }
  else {
    // arrowSpeed*=0.9;
    arrowSpeed = 0.9;
  }
}

function valuesReset() {
  arrowSpeed = 1;
  arrowAngle = getRandomInt(0, 360);
  speedUpFlag = true;
  randomAngle = getRandomInt(300, 400) * getRandomInt(0, 360);
}


//document.getElementById('howManyCircels').addEventListener("click", init());
//document.getElementById('confirm').addEventListener("click", init());

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}




function getPoint(c1, c2, radius, startAngle, finishAngle) {
  return [Math.round(c1 + Math.cos(finishAngle) * radius), Math.round(c2 + Math.sin(finishAngle) * radius)];
}







