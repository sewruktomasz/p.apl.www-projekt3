




function addColorInput() {
    try {


        var howManyColors = parseInt(document.getElementById('howManyZones').value);
        var howManyCircles = parseInt(document.getElementById('howManyCircles').value);
        var divColors = document.getElementById('choosenColors');
        divColors.innerHTML = "";

        if (howManyColors > 0 && howManyColors <= howManyCircles) {

            for (var i = 0; i < howManyColors; i++) {

                newBr = document.createElement('br');
                divColors.appendChild(newBr);

                newLabel = document.createElement('label');
                newLabel.innerHTML = 'Kolor ' + (i + 1) + ":";
                newLabel.setAttribute('for', 'color' + i);
                divColors.appendChild(newLabel);

                newColor = document.createElement('input');
                newColor.setAttribute('id', 'color' + i);
                newColor.setAttribute('value', getRandomColor());
                divColors.appendChild(newColor);
            }
        }
        else {
            divColors.innerHTML = "Podaj prawidłową wartość!"
        }
    }
    catch(error){
        console.error(error);
    }


};

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}



document.getElementById('confirm').addEventListener("click", addColorInput);
addColorInput();

